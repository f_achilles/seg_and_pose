net=vl_simplenn_move(net,'gpu');
makeMovie = true;
% Define test set
testIndices = find(imdb.images.set == 2);
poseFig = figure('Position',[50 50 800 600]);
[az,el]=view(3);
if makeMovie
    movObj = VideoWriter('3DposeSynth_demo.avi');
    open(movObj)
end
for iSample = testIndices
    im = imdb.images.data(:,:,:,iSample);
    lbl = imdb.images.labels(:,:,:,iSample);
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn(net, gpuArray(im), [], [], ...
      'disableDropout', true, ...
      'conserveMemory', false, ...
      'sync', false) ;
    joints = squeeze(gather(res(end-1).x));
    GTjoints = squeeze(lbl);
    % depth map
    subplot(1,2,1)
    imagesc(im(:,:,3),[0 1]);
    axis image
    title('depth map')
    % ground truth pose
    subplot(1,2,2,'align')
    GTjoints = reshape(GTjoints,3,[]);
    drawEvalSkel(reshape(GTjoints',[],1),'*g','3d','Synth');
    GTjoints = reshape(GTjoints,[],1);
    text(double(GTjoints(1:3:end)),double(GTjoints(3:3:end)),double(1-GTjoints(2:3:end))...
        ,num2str((1:numJoints)'));
%     axis image vis3d
%     view(2)
    title('ground truth pose (green) vs. estimated pose (blue)')
    % estimated pose
    subplot(1,2,2)
    joints = reshape(joints,3,[]);
    hold on
    drawEvalSkel(reshape(joints',[],1),'*b','3d','Synth');
    joints = reshape(joints,[],1);
%     text(double(joints(1:3:end)),double(joints(2:3:end)),double(joints(3:3:end))...
%         ,num2str((1:20)'));
    hold off
    axis([0 1 0 1 0 1])
    axis vis3d
%     view(az+iSample, el);
    view([0,-1,0])
    drawnow

if makeMovie
    writeVideo(movObj, getframe(poseFig))
end

end
if makeMovie
    close(movObj)
end

