function handleVec = drawEvalSkel(joints,str,optdim,optDataset)
if exist('optdim','var')
    if strcmp(optdim,'3d')
        make3d = true;
    elseif strcmp(optdim,'2d')
        make3d = false;
    else
        error('Put dimension option to either ''3d'' or ''2d''.')
    end
else
    make3d=false;
end
if exist('optDataset','var')
    if strcmp(optDataset,'EVAL')
    J=[1     1     3     4     1    7     8     1     1     12     16 ;
       2     3     4     5     7    8     9    12    16     13     17 ];
    elseif strcmp(optDataset,'PDT')
    J=[4     3     3     9     5    10     6    11    7     3     2  1  1  17 13 18 14 19 15 ;
       3     9     5     10    6    11     7    12    8     2     1  17 13 18 14 19 15 20 16];
    elseif strcmp(optDataset,'Synth')
    J=[8     12     12     18   4    6     12    19   5    7     18 19  1 15 16 13 15 17 14 ;
       12     1     18     4    6    10    19    5    7   11     1   1 15 16 13  2 17 14 3];
    else
    end
else
J=[20     1     2     1     8    10     2     9    11     3     4     7     7     5     6    14    15    16    17;
    3     3     3     8    10    12     9    11    13     4     7     5     6    14    15    16    17    18    19];
end

numJoints = 19;
if make3d
    S = [joints(1:numJoints) joints((2*numJoints+1):3*numJoints) 1-joints((numJoints+1):2*numJoints)];
else
    S = [joints(1:20) joints(21:40)];
end
if make3d
    handleVec=plot3(S(:,1),S(:,2),S(:,3),str);
else
    handleVec=plot(S(:,1),S(:,2),str);
end
%rotate(h,[0 45], -180);
set(gca,'DataAspectRatio',[1 1 1])
% axis([0 400 0 400 0 400])

for j=1:size(J,2)%19
    c1=J(1,j);
    c2=J(2,j);
    if make3d
        line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)], [S(c1,3) S(c2,3)]);
    else
        line([S(c1,1) S(c2,1)], [S(c1,2) S(c2,2)]);
    end
end
axis image vis3d
view(3)
end