%% attempt to read MSR Action 3D dataset file

% 
BBWidth = 120;

imdbImageCell   = cell(1);
imdbLabelCell   = cell(1);
imdbMeta        = cell(1);
imdbSet         = cell(1);


trainingSubjects = {'s01','s03','s03','s05','s07','s09'};

msrDir = 'E:\poseEstimationDBs\MSR-Action3D';
skeletonSubdir = 'realXYZSkeleton_20joints';
sequences = dir([msrDir filesep '*.bin']);
%let's always take action 1 for now
for iSeq = 1:numel(sequences)%(27*10)
    ID = sequences(iSeq).name(1:(end-11));
    fprintf('processing sequence %s (%d of %d)\n',ID,iSeq,numel(sequences))
    depthFileid   = [ID '_sdepth.bin'];
    skelFileid    = [ID '_skeleton3D.txt'];

    depthFile = fopen([msrDir filesep depthFileid]);
    bitformat = 'int32';
    tmp = fread(depthFile,3,bitformat);
    bitformat = 'int32=>single';
    % figure
    numFrames = tmp(1);
    container = zeros(240,320,numFrames,'single');
    for iFrame = 1:numFrames
        tmp1 = fread(depthFile,[320 240],bitformat)/200;
        container(:,:,iFrame) = tmp1';
        % imagesc(tmp1'); axis image
        % pause(0.03)
    end
    fclose(depthFile);

    skelFile = fopen([msrDir filesep skeletonSubdir filesep skelFileid]);
%     xstore = [];
    gtPose = zeros(80,numFrames);
    for iFrame = 1:numFrames
        temp    = textscan(skelFile,'%f32 %f32 %f32 %f32',20);
%         tempX   = min(temp{1},320);
%         tempY   = min(temp{2},240);
        % filter out bad confidence score points
        tempX   = temp{1};
        tempY   = temp{2};
        tempZ   = temp{3};
        quality = temp{4};
        gtPose(:,iFrame) = cat(1,tempX,tempY,tempZ,quality);
%         xstore  = cat(1,xstore,tempX);
    %     plot(temp{1},temp{2},'*')
    end
    fclose(skelFile);
    
%     shiftX = floor(median(xstore));
%     clear xstore;
    % limit shiftX
%     shiftX = max(BBWidth/2+1,shiftX);
%     shiftX = min(320-BBWidth/2,shiftX);
%     % crop input to bounding box
%     container = container(:,(shiftX-BBWidth/2):(shiftX+BBWidth/2-1),:);
%     % adjust gt pose
%     gtPose(1:20,:) = gtPose(1:20,:)-shiftX;
    
    % downsample container (which is 240x120)
%     imdbImagePart = zeros(20,10,1,numFrames,'single');
%     for iFrame = 1:numFrames
%         for iRow = 1:20
%             for iCol = 1:10
%                 imdbImagePart(iRow,iCol,1,iFrame) = mean(reshape(container(((iRow-1)*12+1):(iRow*12),((iCol-1)*12+1):(iCol*12),iFrame),1,[]));
%             end
%         end
%     end
    container = single(imresize(container,[120 160],'bilinear'));
    imdbImagePart = reshape(container,120,160,1,[]);
    imdbImageCell{1,1,1,end+1} = imdbImagePart;
    imdbLabelCell{1,1,1,end+1} = reshape(gtPose,1, 1, size(gtPose,1),[]);
%     imdbMeta{end+1} = shiftX;
    if any(strcmp(trainingSubjects,ID(5:7)))
        imdbSet{end+1} = 1*ones(1,numFrames); %train set
    else
        imdbSet{end+1} = 2*ones(1,numFrames); %val set
    end
end
imdbImageCell = imdbImageCell(1,1,1,2:end);
imdbLabelCell = imdbLabelCell(1,1,1,2:end);
imdbSet       = imdbSet(2:end);

imdb.images.data = single(cell2mat(imdbImageCell));
imdb.images.labels = single(cell2mat(imdbLabelCell));
N = size(imdb.images.data,4);
imdb.images.set = single(cell2mat(imdbSet));
clear imdbImageCell imdbLabelCell imdbSet container
% imdb.meta.shiftX = cell2mat(imdbMeta(2:end));
imdb.meta.boundingBox = [240 120];
imdb.meta.patchSize = [12 12];


%% make some test images
numSamples = 20;
trainIdx = find(imdb.images.set == 1);
randTrainIdx = randperm(numel(trainIdx),numSamples);
for iTrainSample = 1:numSamples
    im = imdb.images.data(:,:,:,trainIdx(randTrainIdx(iTrainSample)));
    lbl = imdb.images.labels(1,1,:,trainIdx(randTrainIdx(iTrainSample)));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_scnseg(net,gpuArray(im));
    joints = squeeze(gather(res(end-1).x));
    GTjoints = squeeze(lbl);
    figure('position',[100 200 1400 800]);
    % depth map
    subplot(1,3,1)
    imagesc(im);
    axis image
    title('depth map')
    % ground truth pose
    subplot(1,3,2)
    drawKinectSkel(GTjoints,'*g','3d');
%     plot(GTjoints(1:20),GTjoints(21:end),'*g')
    set(gca,'ydir','reverse')
    axlimsGT = axis;
    title('ground truth pose')
    % estimated pose
    subplot(1,3,3)
    drawKinectSkel(joints,'*b','3d');
%     plot(joints(1:20),joints(21:end),'*b')
    set(gca,'ydir','reverse')
    axis(axlimsGT);
    title('estimated pose')
    drawnow
    pause
end