%% read TOF data, POSE data and generate IMDB struct
dataroot    = 'C:\Projects\poseFromRGBD';
subjects    = {'S1','S2'};
modality    = 'TOF';
jointPositions = ['MyPoseFeatures' filesep 'D3_Positions'];

tofFiles    = dir([dataroot filesep subjects{1} filesep modality filesep '*.cdf']);
imdb.images.data = [];
imdb.images.labels = [];
for iSeq = 1:numel(tofFiles)

tmp         = cdfread([dataroot filesep subjects{1} filesep modality filesep tofFiles(iSeq).name]);
tmpJoints   = cdfread([dataroot filesep subjects{1} filesep jointPositions filesep tofFiles(iSeq).name]);
obj.Index = tmp{1};
obj.Indicator = boolean(tmp{2});

obj.Range = tmp{3};
obj.Intensity = tmp{4};

obj.Flag = true;

images = obj.Range(:,:,unique(obj.Index));
poses = tmpJoints{1}(obj.Indicator,:);
sz = size(images);
szPos = size(poses);
images(images < 1.75) = 0;
imdb.images.data = cat(4,imdb.images.data,reshape(images,[sz(1),sz(2),1,sz(3)]));

imdb.images.labels = cat(4,imdb.images.labels,reshape(poses',[1,1,szPos(2),szPos(1)]));
disp(size(imdb.images.data))
% test plots
% figure; plot(obj.Index)
% how can frame number and range-frame number not fit?
% are depth and intensity taken alternatingly?

% figure; plot(obj.Indicator)
% indicator is binary 0/1

% rangeImg = obj.Range(:,:,obj.Index(1));
% rangeImg(rangeImg<1.75)=NaN;
% % depthMapFig = figure; imagesc(rangeImg); axis image
% % figure; surf(-double(rangeImg)*100,'facecolor','interp','edgecolor','none');
% camlight
% lighting gouraud
% axis image vis3d
% % range is in [m]
% 
% irImg = obj.Intensity(:,:,obj.Index(1));
% figure; imagesc(irImg); axis image
% IR-intensity is uint32, strong reflection by tracking-markers!
% From Data Sheet, website: http://www.mesa-imaging.ch/products/sr4000/
% "Value above 32767 indicates saturation"

% Visualize full sequence:
% N = size(obj.Range,3);
% figure
% for iFrame = 1:N
% rangeImg = obj.Range(:,:,iFrame);
% rangeImg(rangeImg<1.75)=NaN;
% imagesc(rangeImg); axis image
% pause(0.03)
% end


end % SEQUENCES

% function obj = H36MTOFDataAccess(Subject, Action, SubAction)
%       db = H36MDataBase.instance();
%       
% 			try 
% 				filename2 = db.getFileName(Subject,Action,SubAction);
% 				tmp = cdfread([db.exp_dir db.getSubjectName(Subject) filesep 'TOF' filesep filename2 '.cdf'], 'Variable',{'RangeFrames','Indicator','Index','IntensityFrames'});
% 				obj.Range = tmp{1};
% 				obj.Intensity = tmp{4};
% 				obj.Index = tmp{3};
% 				obj.Indicator = tmp{2};
% 				obj.Flag = true;
% 			catch e
% 				disp('TOF not found!');
% 				obj.Flag = false;
% 			end
%       
% end
%     
% function [Range obj Intensity] = getFrame(obj, frames)
%         Range = obj.Range(:,:,obj.Index(frames));
%         Intensity = obj.Intensity(:,:,obj.Index(frames));
%   Intensity(Intensity>2300) = 2300;
% end

%% define train and test set
N = size(imdb.images.data,4);
imdb.images.set = ones(1,N);
imdb.images.set(23760:end)=2;

%% visualize results
load('C:\Projects\poseFromRGBD\H80Kcode_v1\skel.mat')
rb = H36MRenderBody(skel,'Style','sketch','ColormapType','left-right');
numSamples = 10;
trainIdx = find(imdb.images.set == 1);
randTrainIdx = randperm(numel(trainIdx),numSamples);
for iTrainSample = 1:numSamples
    im = imdb.images.data(:,:,:,trainIdx(randTrainIdx(iTrainSample)));
    lbl = imdb.images.labels(1,1,:,trainIdx(randTrainIdx(iTrainSample)));
    net.layers{end}.class = gpuArray(lbl);
    res = vl_simplenn_scnseg(net,gpuArray(im));
    joints = squeeze(gather(res(end-1).x))';
    figure('position',[100 200 1400 800]);
    % depth map
    subplot(1,3,1)
    imagesc(im);
    axis image
    title('depth map')
    % ground truth pose
    subplot(1,3,2)
    rb.render3D(squeeze(lbl)'); %gtd3p.Ftrain(frno,:));
    view(3)
    title('ground truth pose')
    axis image vis3d on
    % estimated pose
    subplot(1,3,3)
    rb.render3D(joints);
    view(3)
    title('estimated pose')
    axis image vis3d on
    drawnow
    pause
end

%% map results into TOF coordinate system
db = H36MDataBase.instance();
s=1; % subject 1
cam2 = getCamera(db, s, 2);
skel3dPos = reshape(lbl,3,[])*1000;
% SR-4000 camera parameters:
% https://support.dce.felk.cvut.cz/mediawiki/images/1/18/Dp_2011_smisek_jan.pdf
%
% focal length: 257.6 px
% c = central point: [54.6 91.7]
% k = radial coefficients: [-0.9135 -0.5484 0]
% p = tangential coefficients: [0.04389 -0.008816]
f = 257.6;
% c = [144-54.6 176-91.7];
% fine tune c such that the projection looks good O.o
c = [100 75];
k = [-0.9135 -0.5484 0];
p = [0.04389 -0.008816];
[PX D] = ProjectPointRadial(skel3dPos', cam2.R, cam2.T, f, c, k, p);
figure
imagesc(im); axis image
hold on
plot(PX(:,1),PX(:,2),'*')
hold off

% show mapped bounding box
o = 100; %millimeters
minx = min(skel3dPos(1,:))-o;
maxx = max(skel3dPos(1,:))+o;
miny = min(skel3dPos(2,:))-o;
maxy = max(skel3dPos(2,:))+o;
minz = min(skel3dPos(3,:))-o;
maxz = max(skel3dPos(3,:))+o;
% left/right, upper/lower, frontal/distal corner of bounding box
luf = [minx miny minz];
ruf = [maxx miny minz];
llf = [minx maxy minz];
rlf = [maxx maxy minz];
lud = [minx miny maxz];
rud = [maxx miny maxz];
lld = [minx maxy maxz];
rld = [maxx maxy maxz];
bbox = [luf;ruf;llf;rlf;lud;rud;lld;rld];
bbox = ProjectPointRadial(bbox, cam2.R, cam2.T, f, c, k, p);
figure
imagesc(im); axis image
hold on
plot(bbox(:,1),bbox(:,2),'^k')
[PX D] = ProjectPointRadial(skel3dPos', cam2.R, cam2.T, f, c, k, p);
plot(PX(:,1),PX(:,2),'*')
hold off

% map depth frame to pointcloud
[colIdx,rowIdx] = meshgrid(1:176,1:144);
pc(:,:,3) = im;
pc(:,:,1) = (colIdx-176/2).*im/f;
pc(:,:,2) = (rowIdx-144/2).*im/f;
pc=double(pc);
invalidPxMask = ones(size(im),'double');
invalidPxMask(im<4) = NaN;
figure;
surf(pc(:,:,1),pc(:,:,2),pc(:,:,3).*invalidPxMask,'edgecolor','none')
axis vis3d image
camlight
lighting gouraud
% show bbox in pointcloud
bbox_cam2 = (bsxfun(@minus,bbox,cam2.T))*cam2.R';
luf = bbox_cam2(1,:);
ruf = bbox_cam2(2,:);
llf = bbox_cam2(3,:);
rlf = bbox_cam2(4,:);
lud = bbox_cam2(5,:);
rud = bbox_cam2(6,:);
lld = bbox_cam2(7,:);
rld = bbox_cam2(8,:);
line([luf(1) ruf(1)],[luf(2) ruf(2)],[luf(3) ruf(3)])


% cameraParameters %CV toolbox has camera undistortion --> but test and see
% the result without correction!
im = imdb.images.data(:,:,:,iFrame);

% Visualize full sequence:
N = 300;
figure
for iFrame = (1:N)
im = imdb.images.data(:,:,:,iFrame);
imagesc(im); axis image
hold on
lbl = squeeze(imdb.images.labels(:,:,:,iFrame));
skel3dPos = reshape(lbl,3,[])*1000;
[PX D] = ProjectPointRadial(skel3dPos', cam2.R, cam2.T, f, c, k, p);
plot(PX(:,1),PX(:,2),'*')
hold off
pause(0.03)
end