function jointsPos = readSynthJoints(jointPosFilename)
fid = fopen(jointPosFilename);
numJoints = str2double(fgetl(fid));
assert(numJoints == 19, 'Number of joints is not 19! Check joint position files.');
A = textscan(fid,'%s %f %f %f');
jointsPos(1,:) = 1000*A{2};
jointsPos(2,:) = 1000*A{3};
jointsPos(3,:) = 1000*A{4};
fclose(fid);
end