function [net, info] = cnn_pascalSeg(imdb, varargin)
% cnn_scnseg
% Test architecture for semantic segmentation on NYU2 dataset

% version7:
% Fully and Conv trained together from the beginning
% Fully has 100times higher learning rate
% Validation-set consists of (random) 10% of the augmented data
% binaryTrainVec = imdb.images.set==1;
% rng('default')
% randomIndices = randperm(numel(binaryTrainVec));
% imdb.images.set(binaryTrainVec(randomIndices(1:(ceil(numel(randomIndices)/10))))) = 2;

opts.dataDir = fullfile('..\data') ;
opts.expDir = fullfile('C:\Projects\CombinedSegmentationAndPoseEstimation\data\networks\v3') ;
opts.imdbPath = fullfile(opts.expDir, 'NYU2scale10TrainingAllLabels4ClassesExp.mat');
opts.train.batchSize = 20 ;
opts.train.numEpochs = 250 ;
opts.train.continue = true ;
opts.train.useGpu = true ;
% opts.train.learningRate = [(0.0001)+(0.001-0.0001)*exp(-(0:30))/(1-exp(-30)) 0.0001*ones(1,20)] ;
opts.train.learningRate = 0.005 ;
opts.train.expDir = opts.expDir ;
opts.continue = false;
opts.train.sync = false;
opts.train.plotDiagnostics = false ;
opts.train.use15FPSpredecessor = false;
opts.train.use10FramesWindow = false ;
opts.train.use5FramesWindow = true ;
opts.train.pretrainNet = '';
opts.train.conserveMemory = true ;
opts = vl_argparse(opts, varargin) ;

% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------

% if exist(opts.imdbPath, 'file') && ~exist('imdb','var')
%     imdb = load(opts.imdbPath) ;
%     % convert struct data to 4D matrices format
%     imdb = convertTo4dStruct(imdb.imdb);
% elseif ~exist('imdb','var')
%     error('No training data found!')
% end

% --------------------------------------------------------------------
%                                                   Network definition
% --------------------------------------------------------------------

% networkDef_standard_200resInput
% networkDefinition_standard
% networkDefinition_standard_singleOutput
netDef_pascal_2
% networkDefinition_NoLCN_DualOutput
% networkDefinition_4ChannelsToFully
% networkDefinition_4ChannelsToFully_4FCLayers
% networkDefinition_8ChannelsToFully_4FCLayers
% networkDefinition_1ChannelToFully_4FCLayers

if ~strcmp(opts.train.pretrainNet,'')
    load(opts.train.pretrainNet)
end
% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

trainedNets = dir([opts.expDir filesep 'net-epoch-*.mat']);
if ~isempty(trainedNets)
  disp(['Loading ' opts.expDir filesep trainedNets(end).name ' ...'])
  load([opts.expDir filesep trainedNets(end).name])
  opts.train.continue = false;
end

%%%
% Pixelweise mean/sdev
%%%
% Take the mean out
if ~isfield(net, 'meta')
meanImage = mean(imdb.images.data(:,:,:,imdb.images.set==1),4);
% meanImage = zeros(100,100,2,'single');
net.meta.meanImage = meanImage;
end
meanImage = net.meta.meanImage;
imdb.images.data = bsxfun(@minus, imdb.images.data, meanImage) ;

% divide by the standard variation [factor (1/(n-1))]
% stdDevImage = std(imdb.images.data(:,:,:,boolean(imdb.meta.valid10Frames) & imdb.images.set==1),0,4);
stdDevImage = ones(200,200,2,'single');
% imdb.images.data = bsxfun(@rdivide, imdb.images.data, stdDevImage) ;
net.meta.stdDevImage = stdDevImage;

%%%
% Convolutional mean/sdev
%%%
% % Take the mean out
% meanImageIR = mean(reshape(imdb.images.data(:,:,1,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,1,:) = bsxfun(@minus, imdb.images.data(:,:,1,:), meanImageIR) ;
% meanImageDepth = mean(reshape(imdb.images.data(:,:,2,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,2,:) = bsxfun(@minus, imdb.images.data(:,:,2,:), meanImageDepth) ;
% net.meta.meanImage = [meanImageIR meanImageDepth];
% % divide by the standard variation [factor (1/(n-1))]
% stdDevImageIR = std(reshape(imdb.images.data(:,:,1,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,1,:) = bsxfun(@rdivide, imdb.images.data(:,:,1,:), stdDevImageIR) ;
% stdDevImageDepth = std(reshape(imdb.images.data(:,:,2,boolean(imdb.meta.clean10Frames) & imdb.images.set==1),1,[]));
% imdb.images.data(:,:,2,:) = bsxfun(@rdivide, imdb.images.data(:,:,2,:), stdDevImageDepth) ;
% net.meta.stdDevImage = [stdDevImageIR stdDevImageDepth];


[net, info] = cnn_train_pascal(net, imdb, @getBatch, ...
    opts.train) ;
end


% --------------------------------------------------------------------
function [im, labels] = getBatch(imdb, batch)%,boolFlip)
% --------------------------------------------------------------------
im = imdb.images.data(:,:,:,batch) ;
% if boolFlip
% im(:,:,:,1:(floor(numel(batch)/2))) = fliplr(im(:,:,:,1:(floor(numel(batch)/2))));
% end
labels = imdb.images.labels(:,:,1,batch) ;
end

