function Y = vl_skeletonLoss(X,c,dzdy)
sz_ = [size(X,1) size(X,2) size(X,3) size(X,4)];
numJoints = 3*20;
%%%
% Version with NaNs in the ground truth jointXYZ positions 'c'
%%%
validPts = ~isnan(c);
c_clean = gpuArray(zeros(size(c),'single'));
c_clean(validPts) = c(validPts);
if nargin < 3
    % forward prop
    % jointXYZ-loss
    Y = sum(sum(sum(((X(:,:,1:numJoints,:)-c_clean).*single(validPts)).^2,1),2),3);
    % length-loss
    jointDiffs  = (X(:,:,firstInds,:)-X(:,:,secondInds,:)).^2;
    lengths     = sqrt(jointDiffs(:,:,1:3:end,:)+jointDiffs(:,:,2:3:end,:)+jointDiffs(:,:,3:3:end,:));
else
    % backward prop
    Y = dzdy * 2*(X(:,:,1:numJoints,:)-c_clean).*single(validPts);
end
Y =  bsxfun(@rdivide,Y,eps+sum(sum(sum(validPts,1),2),3));
end